---
title: Ubuntu Web
description: Ubuntu Web Remix combines the power of Ubuntu & Firefox to give you a powerful, free/libre web OS.
background: "images/bg_image.png"
logo: "images/logo_circle.svg"
---

