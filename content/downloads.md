---
title: "Get it"
draft: false
---

Direct download: [DarkPenguin](http://linux.darkpenguin.net/distros/ubuntu-unity/ubuntu-web/20.04.3)

Torrent: [FOSS Torrents](https://fosstorrents.com/thankyou/?name=ubuntu-web&cat=Current%20Editions&id=0)
